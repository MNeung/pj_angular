import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _registerUrl = 'http://localhost:8000/api/register';
  private _loginUrl = 'http://localhost:8000/api/login';
  private _changePassUrl = 'http://localhost:8000/api/changepass';
  private _todoListUrl = 'http://localhost:8000/api/todos/';
  private _todoUrl = 'http://localhost:8000/api/todo';
  private _todoUpdateUrl = 'http://localhost:8000/api/updatetodo';
  private _todoDeleteUrl = 'http://localhost:8000/api/deletetodo/';
  constructor(private http: HttpClient) { }

  registerUser(user) {
    return this.http.post<any>(this._registerUrl, user);
  }

  loginUser(user) {
    return this.http.post<any>(this._loginUrl, user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  changePass(data) {
    return this.http.put<any>(this._changePassUrl, data);
  }

  // getToken() {
  //   return localStorage.getItem('token');
  // }

  // logoutUser(user) {
  //   return this.http.post<any>(this._loginUrl, user);
  // }

  todoList(id) {
    return this.http.get<any[]>(this._todoListUrl + id);
  }

  todoAdd(todo) {
    return this.http.post<any>(this._todoUrl, todo);
  }

  todoUpdate(todo) {
    return this.http.put<any>(this._todoUpdateUrl, todo);
  }

  todoDelete(id) {
    return this.http.delete<any>(this._todoDeleteUrl + id);
  }
}
