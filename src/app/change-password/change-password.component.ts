import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  password = {
    username: '',
    currentPassword: '',
    newPassword: ''
  }
  constructor(private _auth: AuthService, private _router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  change() {
    this.password.username = localStorage.getItem('username');
    this._auth.changePass(JSON.stringify(this.password))
      .subscribe(
        res => {
          localStorage.clear();
          this._router.navigate(['/']);
        },
        err => console.log(err),
      );

  }

  cancel() {
    this.password.currentPassword = '';
    this.password.newPassword = '';
    this._router.navigate(['/todo']);
  }
}
