import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
// import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginUserData = {
    username: '',
    password: ''
  };
  constructor(private _auth: AuthService, private _router: Router, private toastr: ToastrService) { }
  // constructor(private _auth: LoginService) { }

  ngOnInit(): void {
  }

  cliclLogin() {
    this._auth.loginUser(JSON.stringify(this.loginUserData))
      .subscribe(
        res => {
          if (res.username && res.token) {
            localStorage.setItem('token', res.token);
            localStorage.setItem('username', res.username);
            this._router.navigate(['/todo']);
          } else {
            this.toastr.error('username หรือ password ผิด');
          }
        },
        err => console.log(err),
      );
  }

}
