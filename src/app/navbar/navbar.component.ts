import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  username = '';
  login = false;
  public isMenuCollapsed = true;
  constructor(private _router: Router) { }

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    this.login = this.username ? true : false;
  }

  clickMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
  }

  clickLogout() {
    localStorage.clear();
    this._router.navigate(['/']);
    this.login = false;
  }

}
