import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public isCollapsed = false;

  registerUserData = {
    username: '',
    password: '',
    firstname: '',
    lastname: ''
  };

  constructor(private _auth: AuthService, private toastr: ToastrService, private _router: Router,) { }

  ngOnInit(): void {
  }

  registerUser() {
    if (
      this.registerUserData.username &&
      this.registerUserData.password &&
      this.registerUserData.firstname &&
      this.registerUserData.lastname
    ) {
      this._auth.registerUser(JSON.stringify(this.registerUserData))
        .subscribe(
          res => {
            if (
              res.result === 'Username already Exists!!'
            ) {
              this.toastr.error('username ถูกใช้งานแล้ว');
            } else {
              this.toastr.success('register เรียบร้อย');
              setTimeout(() => { this._router.navigate(['/']); }, 3000);
            }
          },
          err => console.log(err),
        );
    } else {
      this.toastr.error('กรุณากรอกข้อมูล');
    }
  }

  cancelRegister() {
    this.registerUserData.username = '';
    this.registerUserData.password = '';
    this.registerUserData.firstname = '';
    this.registerUserData.lastname = '';
    this._router.navigate(['/']);
  }

}
