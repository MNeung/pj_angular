import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todoData = {
    username: '',
    todos: ''
  };
  todoList = [];
  todo = {
    id: '',
    valueTodo: ''
  }
  constructor(private _router: Router, private _auth: AuthService, private toastr: ToastrService) { }

  ngAfterViewInit(): void {
    this.todoData.username = localStorage.getItem('username');
    this._auth.todoList(localStorage.getItem('username'))
      .subscribe(
        resTodo => {
          // localStorage.setItem('todo', JSON.stringify(resTodo));
          // console.log('res', resTodo);
          this.todoList = resTodo;
          // localStorage.setItem('username', res.token);
        },
        err => console.log(err),
      );
  }

  ngOnInit(): void {
  }

  addTodo() {
    // console.log('todoData', this.todoData);
    // console.log('localStorage.getItem', localStorage.getItem('username'));
    if (this.todoData.username && this.todoData.todos) {
      this._auth.todoAdd(JSON.stringify(this.todoData))
        .subscribe(
          res => {
            location.reload();
          },
          err => console.log(err),
        );
    } else {
      this.toastr.error('กรุณากรอกข้อความ');
    }
  }

  updateTodo(index) {
    this.todo.id = this.todoList[index]._id
    this.todo.valueTodo = this.todoList[index].todos
    this._auth.todoUpdate(JSON.stringify(this.todo))
    .subscribe(
      res => {
        location.reload();
      },
      err => console.log(err),
    );
  }

  deleteTodo(index) {
    // console.log("index", index);
    // console.log("todoList", this.todoList[index]._id);
    this._auth.todoDelete(this.todoList[index]._id)
      .subscribe(
        res => {
          location.reload();
          // console.log("res selete", res);
        },
        err => console.log(err),
      );
  }
}
